# Implementation of a CPR-AMG Two-Stage Preconditioner #

A CPR-AMG Two-Stage Preconditioner promises to robustly and efficiently solve ill-conditioned linear systems for unknowns in pressure and saturations typically occurring in subsurface flow and transport problems. Currently, PFLOTRAN only uses One-Stage Preconditioners to solve such linearised systems. This project will implement a CPR-AMG Two-Stage Preconditioner and test its robustness, efficiency and scalability for various choices of Preconditioners.

A CPR-AMG Two-Stage Preconditioner exploits the algebraic structure of a fully implicit formulation of a  coupled system to robustly and efficiently solve it, by extracting its elliptic subsystem, preconditioning and solving it with residuals associated to this solution corrected by a second preconditioner that recovers information contained in the original system.

### Specific Tasks to Execute the Project ###

* Prototype a Two-Stage Preconditioner procedure using PETSc.

* Analyse convergence behaviour of a Two-Stage Preconditioner with various preconditioner choices for 4 different simulations to see how heterogeneity in porosity and permeability as well as energy flow and transport impact its performance.

* Use an Algebraic Multi Grid solver as the first preconditoner and analyse convergence behaviour for the 4 above mentioned different simulations.

* Scalability test for more than 32+ procs